import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditComandaPageRoutingModule } from './edit-comanda-routing.module';

import { EditComandaPage } from './edit-comanda.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditComandaPageRoutingModule,
    ReactiveFormsModule

  ],
  declarations: [EditComandaPage]
})
export class EditComandaPageModule {}
