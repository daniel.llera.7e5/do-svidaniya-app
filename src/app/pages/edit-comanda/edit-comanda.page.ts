import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-edit-comanda',
  templateUrl: './edit-comanda.page.html',
  styleUrls: ['./edit-comanda.page.scss'],
})
export class EditComandaPage implements OnInit {

  editForm: FormGroup;
  id: String;
  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute,
        private router: Router, public formBuilder: FormBuilder) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.dataService.getComanda(this.id).subscribe(
      res => {
      this.editForm = this.formBuilder.group({
        name: [res['name']],
        date: [res['date']],
        state: [res['state']],
        table: [res['table']],
        items: [res['items']],
        price: [res['price']]
      })
    });
  }
 
  ngOnInit() {
    this.editForm = this.formBuilder.group({
      name: [''],
      date: [''],
      state:[''],
      table: [''],
      items:[],
      price: ['']
    })
  }
 
  onSubmit() {
    this.dataService.updateComanda(this.id, this.editForm.value);
  }
 

}
