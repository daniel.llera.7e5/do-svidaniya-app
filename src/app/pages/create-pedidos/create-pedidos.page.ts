import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-create-pedidos',
  templateUrl: './create-pedidos.page.html',
  styleUrls: ['./create-pedidos.page.scss'],
})
export class CreatePedidosPage implements OnInit {

  comandaForm: FormGroup;

  constructor(private dataService: DataService,
    public formBuilder: FormBuilder,   
    private router: Router) { }
 
  ngOnInit() {
    this.comandaForm = this.formBuilder.group({
      calle: [''],
      numero: [''],
      piso: [''],
      state:[''],
      items:[],
      price: ['']
    })
  }
 
  onSubmit() {
    if (!this.comandaForm.valid) {
      return false;
    } else {
      this.dataService.createPedido(this.comandaForm.value)
      .then(() => {
        this.comandaForm.reset();
        this.router.navigate(['/tabs']);
      }).catch((err) => {
        console.log(err)
      });
    }
  }
 

}
