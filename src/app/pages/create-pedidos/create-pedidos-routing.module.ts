import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreatePedidosPage } from './create-pedidos.page';

const routes: Routes = [
  {
    path: '',
    component: CreatePedidosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreatePedidosPageRoutingModule {}
