import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreatePedidosPageRoutingModule } from './create-pedidos-routing.module';

import { CreatePedidosPage } from './create-pedidos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreatePedidosPageRoutingModule,
    ReactiveFormsModule

  ],
  declarations: [CreatePedidosPage]
})
export class CreatePedidosPageModule {}
