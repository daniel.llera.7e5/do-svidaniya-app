import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateComandasPage } from './create-comandas.page';

const routes: Routes = [
  {
    path: '',
    component: CreateComandasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateComandasPageRoutingModule {}
