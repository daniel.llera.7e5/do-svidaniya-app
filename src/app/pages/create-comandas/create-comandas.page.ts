import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { EventsService } from 'src/app/services/events.service';
import { MenuItem } from '../../interfaces/interfaces';

@Component({
  selector: 'app-create-comandas',
  templateUrl: './create-comandas.page.html',
  styleUrls: ['./create-comandas.page.scss'],
})
export class CreateComandasPage implements OnInit {

  chosenItems = [];
  finalOrder = [];
  meatItems = [];
  dessertItems = [];
  drinkItems = [];
  saladItems = [];
  soupItems = [];
  pastaItems = [];
  seafoodItems = [];
  allItems = [];
  selectableItems = [];
  total = 0;
  comanda;


  constructor(private dataService: DataService, public events: EventsService, private menu: MenuController, private route: ActivatedRoute) {
    
    this.events.getObservable().subscribe((data) => {
      console.log('Data received', data);
    });

    this.route.queryParams.subscribe(params => {
      if(params && params.comanda) {
        this.comanda = JSON.parse(params.comanda)
      }
    })
  }
 
  deleteComanda(selectedItem) {
    console.log("DELETE " + selectedItem.name)
    this.total = 0;
    var count = 0;
    this.comanda.items.forEach(item => {
      count++;
      if (item.name == selectedItem.name) {
        this.comanda.items.splice(count - 1, 1);
      }
    });

    this.comanda.items.forEach(item => {
      this.total += (item.quantity * item.price)
    });

    this.comanda.total = this.total;
  }
  
  openMenu() {
    this.menu.enable(true, 'order');
    this.menu.open('order');
  }

  ngOnInit() {
    this.getAllMenuItems();
    setTimeout(() => {
      this.selectableItems.push(...this.meatItems, ...this.dessertItems, ...this.drinkItems, ...this.saladItems, ...this.soupItems, ...this.pastaItems, ...this.seafoodItems);
      this.allItems = this.selectableItems;
    }, 2000);
  }
  
  getAllMenuItems() {
    // Meats from Database
    this.dataService.getMeats().subscribe(
      res => {
        this.meatItems = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ... item.payload.doc.data() as MenuItem
          };
        }) 
      }
    )

    // Desserts from Database
    this.dataService.getDesserts().subscribe(
      res => {
        this.dessertItems = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ... item.payload.doc.data() as MenuItem
          };
        }) 
      }
    )

    // Drinks from Database
    this.dataService.getDrinks().subscribe(
      res => {
        this.drinkItems = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ... item.payload.doc.data() as MenuItem
          };
        }) 
      }
    )

    // Salads from Database
    this.dataService.getSalads().subscribe(
      res => {
        this.saladItems = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ... item.payload.doc.data() as MenuItem
          };
        }) 
      }
    )

    // Pasta from Database
    this.dataService.getPastas().subscribe(
      res => {
        this.pastaItems = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ... item.payload.doc.data() as MenuItem
          };
        }) 
      }
    )

    // Seafood from Database
    this.dataService.getSeafoods().subscribe(
      res => {
        this.seafoodItems = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ... item.payload.doc.data() as MenuItem
          };
        }) 
      }
    )

    // Soups from Database
    this.dataService.getSoups().subscribe(
      res => {
        this.soupItems = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ... item.payload.doc.data() as MenuItem
          };
        }) 
      }
    )
  }

  chooseItem(item) {
    console.log(this.comanda);

    this.chosenItems.push(item);
  }

  activateFilter(filter) {
    var filterElement = document.getElementById(filter);

    if (filter == "desserts") {
      if (filterElement.style.backgroundColor == 'rgb(203, 255, 255)') {
        filterElement.style.backgroundColor = "#a7a7a0";
        this.selectableItems = this.allItems;
        this.events.publishSomeData({
          update: 'list'
        });
      } else {
        filterElement.style.backgroundColor = 'rgb(203, 255, 255)';
        this.selectableItems = this.dessertItems;
        this.events.publishSomeData({
          update: 'list'
        });
      }
    } else if (filter == "drinks") {
      if (filterElement.style.backgroundColor == 'rgb(253, 177, 150)') {
        filterElement.style.backgroundColor = "#a7a7a0";
        this.selectableItems = this.allItems;
        this.events.publishSomeData({
          update: 'list'
        });
      } else {
        filterElement.style.backgroundColor = 'rgb(253, 177, 150)';
        this.selectableItems = this.drinkItems;
        this.events.publishSomeData({
          update: 'list'
        });
      }
    } else if (filter == "meats") {
      if (filterElement.style.backgroundColor == 'rgb(243, 214, 219)') {
        filterElement.style.backgroundColor = "#a7a7a0";
        this.selectableItems = this.allItems;
        this.events.publishSomeData({
          update: 'list'
        });
      } else {
        filterElement.style.backgroundColor = 'rgb(243, 214, 219)';
        this.selectableItems = this.meatItems;
        this.events.publishSomeData({
          update: 'list'
        });
      }
    } else if (filter == "pasta") {
      if (filterElement.style.backgroundColor == 'rgb(241, 232, 180)') {
        filterElement.style.backgroundColor = "#a7a7a0";
        this.selectableItems = this.allItems;
        this.events.publishSomeData({
          update: 'list'
        });
      } else {
        filterElement.style.backgroundColor = 'rgb(241, 232, 180)';
        this.selectableItems = this.pastaItems;
        this.events.publishSomeData({
          update: 'list'
        });
      }
    } else if (filter == "salads") {
      if (filterElement.style.backgroundColor == 'rgb(144, 243, 144)') {
        filterElement.style.backgroundColor = "#a7a7a0";
        this.selectableItems = this.allItems;
        this.events.publishSomeData({
          update: 'list'
        });
      } else {
        filterElement.style.backgroundColor = 'rgb(144, 243, 144)';
        this.selectableItems = this.saladItems;
        this.events.publishSomeData({
          update: 'list'
        });
      }
    } else if (filter == "seafood") {
      if (filterElement.style.backgroundColor == 'rgb(124, 222, 255)') {
        filterElement.style.backgroundColor = "#a7a7a0";
        this.selectableItems = this.allItems;
        this.events.publishSomeData({
          update: 'list'
        });
      } else {
        filterElement.style.backgroundColor = 'rgb(124, 222, 255)';
        this.selectableItems = this.seafoodItems;
        this.events.publishSomeData({
          update: 'list'
        });
      }
    } else if (filter == "soups") {
      if (filterElement.style.backgroundColor == 'bisque') {
        filterElement.style.backgroundColor = "#a7a7a0";
        this.selectableItems = this.allItems;
        this.events.publishSomeData({
          update: 'list'
        });
      } else {
        filterElement.style.backgroundColor = 'bisque';
        this.selectableItems = this.soupItems;
        this.events.publishSomeData({
          update: 'list'
        });
      }
    }
  }
    
  onSubmit() {
    this.total = 0;
    var quantity = [];
    this.finalOrder = [];
    quantity = this.comanda.items

    this.chosenItems.forEach(function(value){
      if(quantity.filter(e => e.name === value.name).length > 0){
        var index = quantity.findIndex((obj => obj.name == value.name));
        quantity[index].quantity += 1;
      }
      else {
        quantity.push({
          name: value.name, 
          quantity: 1,
          price: value.price
        });
      }
    });

    this.chosenItems = [];

    this.finalOrder = quantity;

    this.finalOrder.forEach(item => {
      this.total += (item.quantity * item.price)
    });
    console.log(this.finalOrder);
    this.comanda.items = this.finalOrder;
    this.comanda.total = this.total;
    this.openMenu();
  }

  updateOrder() {
    console.log("UPDATINGG")
    this.dataService.updateComanda(this.comanda.id, this.comanda);
    this.menu.enable(false, 'order');
    this.menu.close('order');
  }
}
