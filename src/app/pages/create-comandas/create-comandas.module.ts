import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateComandasPageRoutingModule } from './create-comandas-routing.module';

import { CreateComandasPage } from './create-comandas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateComandasPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [CreateComandasPage]
})
export class CreateComandasPageModule {}
