export interface MenuItem {
    img: string;
    price: number;
    name: string;
    available: boolean;
}