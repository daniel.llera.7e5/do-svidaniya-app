import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  pedidos = [];
  storePedidos = [];
  order = "";

  constructor(private router: Router,private dataService: DataService,private menu: MenuController ) {}
  ngOnInit() {
    this.dataService.getPedidos().subscribe(
      res => {
        this.storePedidos = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ... item.payload.doc.data() as Pedido
          };
        })
      }
    );

    setTimeout(() => {
      var date = new Date().toISOString().split('T')[0];

      this.storePedidos.forEach(pedido => {
        if (this.isItGreaterThan(pedido.date, date, 1)) {
          this.pedidos.push(pedido);
      }});
    }, 3000);
  }
  openEnd(id) {
    console.log("YEPA")
    this.pedidos.forEach(pedido => {
      if (pedido.id == id) {
        this.order = pedido;
      }
    });
    this.menu.open();
    this.menu.open('end');
  }
  deletePedido(id){
    if (window.confirm('Do you want to delete this Pedido?')) {
      this.dataService.deletePedido(id)
    }
  }

  navigate_create() {
    this.router.navigate(['../create-pedidos'])
  }

  changeTime(timeSelected) {
    var date = new Date().toISOString().split('T')[0];
    this.pedidos = [];
    this.storePedidos.forEach(pedido => {
      if (timeSelected == "" && this.isItGreaterThan(pedido.date, date, 1)) {
        this.pedidos.push(pedido);
      } else if (pedido.time == timeSelected && pedido.date == date) {
        this.pedidos.push(pedido);
      }
    });
  }

  isItGreaterThan(date1, date2, days) {
    var firstDate = new Date(date1);
    var secondDate = new Date(date2);
    var time = days * 60 * 60 * 24 * 1000
    if ((secondDate.getTime() - firstDate.getTime()) < time) {
      return true;
    } else {
      return false;
    }
  }

}

export interface Pedido {
  id: string;
  address: string;
  homenumber: string;
  floor: string;
  email: boolean;
  items: string[];
  name: string;
  total: number;
  phone: string;
  time: string;
}

