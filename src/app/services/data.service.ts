import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { Comanda } from '../tab1/tab1.page';
import { Pedido } from '../tab2/tab2.page';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private firestore: AngularFirestore, private router: Router) { }


  createComanda(comanda: Comanda){
    return this.firestore.collection('comandas').add(comanda);
  }
 
  getComandas() {
    return this.firestore.collection('comandas').snapshotChanges();
  }
 
  getComanda(id) {
    return this.firestore.collection('comandas').doc(id).valueChanges();
  }
 
  updateComanda(id, comanda: Comanda) {
    this.firestore.collection('comandas').doc(id).update(comanda)
      .then(() => {
        this.router.navigate(['/tabs']);
      }).catch(error => console.log(error));
  }

  deleteComanda(id) {
    this.firestore.doc('comandas/'+id).delete();
  }

  // Pedidos //

  createPedido(pedido: Pedido){
    return this.firestore.collection('pedidos').add(pedido);
  }
 
  getPedidos() {
    return this.firestore.collection('pedidos').snapshotChanges();
  }
 
  getPedido(id) {
    return this.firestore.collection('pedidos').doc(id).valueChanges();
  }

  updatePedido(id, pedido: Pedido) {
    this.firestore.collection('pedidos').doc(id).update(pedido)
      .then(() => {
        this.router.navigate(['list-books']);
      }).catch(error => console.log(error));
  }
 
  deletePedido(id) {
    this.firestore.doc('pedidos/'+id).delete();
  }

  // Menu Items

  getDesserts() {
    return this.firestore.collection('Menu/Category/Desserts').snapshotChanges();
  }

  getDrinks() {
    return this.firestore.collection('Menu/Category/Drinks').snapshotChanges();
  }

  getMeats() {
    return this.firestore.collection('Menu/Category/Meats').snapshotChanges();
  }

  getPastas() {
    return this.firestore.collection('Menu/Category/Pasta').snapshotChanges();
  }

  getSalads() {
    return this.firestore.collection('Menu/Category/Salads').snapshotChanges();
  }

  getSeafoods() {
    return this.firestore.collection('Menu/Category/Seafood').snapshotChanges();
  }

  getSoups() {
    return this.firestore.collection('Menu/Category/Soups').snapshotChanges();
  }

  getDessert(id) {
    return this.firestore.collection('Menu/Category/Desserts').doc(id).valueChanges();
  }

  getDrink(id) {
    return this.firestore.collection('Menu/Category/Drinks').doc(id).valueChanges();
  }

  getMeat(id) {
    return this.firestore.collection('Menu/Category/Meats').doc(id).valueChanges();
  }

  getPasta(id) {
    return this.firestore.collection('Menu/Category/Pasta').doc(id).valueChanges();
  }

  getSalad(id) {
    return this.firestore.collection('Menu/Category/Salads').doc(id).valueChanges();
  }

  getSeafood(id) {
    return this.firestore.collection('Menu/Category/Seafood').doc(id).valueChanges();
  }

  getSoup(id) {
    return this.firestore.collection('Menu/Category/Soups').doc(id).valueChanges();
  }
}
