import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NavigationExtras} from '@angular/router';
import { DataService } from '../services/data.service';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  comandas = [];
  storeComandas = []
  comand = "";
  constructor(private router: Router ,private dataService: DataService, private menu: MenuController) { }
  
  ngOnInit() {
    this.dataService.getComandas().subscribe(
      res => {
        this.storeComandas = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ... item.payload.doc.data() as Comanda
          };
        })
      }
    )

    setTimeout(() => {
      var date = new Date().toISOString().split('T')[0];

      this.storeComandas.forEach(comanda => {
        if (this.isItGreaterThan(comanda.date, date, 1)) {
          this.comandas.push(comanda);
      }});
    }, 3000);
  }

  deleteComanda(event, id){
    event.stopPropagation();
    if (window.confirm('Do you want to delete this Comanda?')) {
      this.dataService.deleteComanda(id)
    }
  }
 
  navigate_create(event, comanda) {
    event.stopPropagation();
    let navigationExtras: NavigationExtras = {
      queryParams: {
        comanda: JSON.stringify(comanda)
      }
    }
    this.router.navigate(['../create-comandas'], navigationExtras)
  }

  changeTime(timeSelected) {
    var date = new Date().toISOString().split('T')[0];
    this.comandas = [];
    this.storeComandas.forEach(order => {
      if (timeSelected == "" && this.isItGreaterThan(order.date, date, 1)) {
        this.comandas.push(order);
      } else if (order.time == timeSelected && order.date == date) {
        this.comandas.push(order);
      }
    });
  }

  isItGreaterThan(date1, date2, days) {
    var firstDate = new Date(date1);
    var secondDate = new Date(date2);
    var time = days * 60 * 60 * 24 * 1000
    if ((secondDate.getTime() - firstDate.getTime()) < time) {
      return true;
    } else {
      return false;
    }
  }
}

export interface Comanda {
  id: string;
  name: string;
  date: Date;
  state: boolean;
  table: string;
  items: string[];
  price: string;
}
