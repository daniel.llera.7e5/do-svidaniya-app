import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'create-comandas',
    loadChildren: () => import('./pages/create-comandas/create-comandas.module').then( m => m.CreateComandasPageModule)
  },
  {
    path: 'edit-comanda/:id',
    loadChildren: () => import('./pages/edit-comanda/edit-comanda.module').then( m => m.EditComandaPageModule)
  },
  {
    path: 'create-pedidos',
    loadChildren: () => import('./pages/create-pedidos/create-pedidos.module').then( m => m.CreatePedidosPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
