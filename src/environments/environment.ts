// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

export const environment = {
  production: false,
  firebaseConfig: {

    apiKey: "AIzaSyD2O8q2_Kskz8HNoS94BYzpxyT5N1DeBeg",
  
    authDomain: "dosvidaniya-e25d5.firebaseapp.com",
  
    databaseURL: "https://dosvidaniya-e25d5-default-rtdb.europe-west1.firebasedatabase.app",
  
    projectId: "dosvidaniya-e25d5",
  
    storageBucket: "dosvidaniya-e25d5.appspot.com",
  
    messagingSenderId: "444097388300",
  
    appId: "1:444097388300:web:3c74f9e0d5419ee02a3329"
  
  }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
